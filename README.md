# Increment ID

An Inkscape 1.2 extension ( may work with earlier )

Increments svg ids with your choice of prefix. 

Does not account for or correct for existing duplicate ids (Inkscape will ignore by default )

Appears under Extensions>Increment by ID
