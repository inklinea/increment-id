import inkex





class IncrementId(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--ignore_labels_bool", type=inkex.Boolean, dest="ignore_labels_bool", default=False)
        pars.add_argument("--id_prefix_string", type=str, dest="id_prefix_string", default='x')

        pars.add_argument("--increment_start_int", type=int, dest="increment_start_int", default=0)
        pars.add_argument("--zero_padding_int", type=int, dest="zero_padding_int", default=0)

        pars.add_argument("--use_selection_radio", type=str, dest="use_selection_radio", default='selection')


        pars.add_argument("--selection_type_radio", type=str, dest="selection_type_radio", default='path')
    
    def effect(self):

        # Remove Whitespace from id string
        id_prefix_string = self.options.id_prefix_string.strip().replace(' ', '_')

        if self.options.use_selection_radio == 'selection':
            selection_list = self.svg.selected
            if len(selection_list) < 1:
                inkex.errormsg('Nothing Selected')
        else:
            selection_list = self.svg.xpath(f'//svg:{self.options.selection_type_radio}')

        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')
            return

        # Label Objects
        count = self.options.increment_start_int
        for element in selection_list:
            element_id = f'{id_prefix_string}{str(count).zfill(self.options.zero_padding_int)}'
            element.set('id', element_id)
            if not self.options.ignore_labels_bool:
                element.set('inkscape:label', element_id)

            count += 1

        
if __name__ == '__main__':
    IncrementId().run()
